import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputModule } from 'src/app/shared/input/input.module';
import { CartComponent } from './cart.component';

@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    InputModule
  ],
  exports: [
    CartComponent
  ]
})
export class CartModule { }
