import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'src/app/shared/button/button.module';
import { InputModule } from 'src/app/shared/input/input.module';

import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './products.component';

@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    InputModule
  ],
  exports: [
    ProductsComponent
  ]
})
export class ProductsModule { }
